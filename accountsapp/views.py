from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def signin(request):
    print(request.method)

    if request.method == "GET":
        form = AuthenticationForm()

        data = {"signin_form": form}
    else:
        print("Else Loop")
        print(request.POST)
        form = AuthenticationForm(data = request.POST)

        data = {"signin_form": form}

        if form.is_valid():
            print("Form is Valid")
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("signin")
        else:
            print("Form is Invalid")

    return render(request, "accountsapp/signin.html", context = data)


def signup(request):
    print(request.method)

    if request.method == "GET":
        form = UserCreationForm()
        data = {"form": form}
            
        return render(request, "accountsapp/signup.html", context = data)
    else:
        print("Else Loop")
        print(request.POST)

        form = UserCreationForm(data = request.POST)

        data = {"form": form}

        if form.is_valid():
            print("Form is Valid")

            username = form.cleaned_data["username"]

            user = form.save()

            if form is not None:
                return render(request, "accountsapp/signin.html")
            else:
                return render(request, "accountsapp/signup.html", context = data)
        else:
            return render(request, "accountsapp/signup.html", context = data)
def signout(request):
    logout(request)

    return render(request, "accountsapp/signout.html")