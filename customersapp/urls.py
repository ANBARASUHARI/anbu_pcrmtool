from django.urls import path
from . import views

urlpatterns = [
    path("add-customer/", views.add_customer, name="add_customer"),
    path("edit-customer/", views.edit_customer, name="edit_customer"),
    path("view-customer/", views.view_customer, name="view_customer"),
]